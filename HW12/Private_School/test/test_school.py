import random
import string

import pytest

from HW12.Private_School.files.SchoolClass import SchoolClass, StudentsPresenceException
from HW12.Private_School.files.Staff import Staff
from HW12.Private_School.test.conftest import TEST_SCHOOL, TEST_SCHOOL_CLASS


class TestStudents:
    def test_payment_change_when_add_student(self):
        previous_payment = TEST_SCHOOL.monthly_payment_for_each_student()
        TEST_SCHOOL_CLASS.add_student("Test Student")
        new_payment = TEST_SCHOOL.monthly_payment_for_each_student()

        assert new_payment < previous_payment

    def test_payment_change_when_exclude_student(self):
        previous_payment = TEST_SCHOOL.monthly_payment_for_each_student()
        TEST_SCHOOL_CLASS.exclude_student("Vova Ivanov")
        new_payment = TEST_SCHOOL.monthly_payment_for_each_student()

        assert new_payment > previous_payment

    def test_correct_payment_calculation(self):
        actual_payment = TEST_SCHOOL.monthly_payment_for_each_student()
        expected_payment = (
            Staff.sum_of_salaries / SchoolClass.count_of_students_in_all_classes
        )

        assert actual_payment == expected_payment

    def test_student_should_be_unique(self):
        new_student = "".join(random.choices(string.ascii_letters, k=10))
        TEST_SCHOOL_CLASS.add_student(new_student)
        with pytest.raises(StudentsPresenceException) as exc:
            TEST_SCHOOL_CLASS.add_student(new_student)
        assert "This Student is in this class already" in str(exc.value)



from HW12.Private_School.files.SchoolCalculation import SchoolCalculator
from HW12.Private_School.files.SchoolClass import SchoolClass
from HW12.Private_School.files.Staff import Staff

TEST_SCHOOL_STAFF_1 = Staff(name="Erik Peach", position="Director", salary=1000)
TEST_SCHOOL_STAFF_2 = Staff(name="Tom Rom", position="Vice Director", salary=900)
TEST_SCHOOL_STAFF_3 = Staff(name="Anna Rom", position="teacher", salary=900)
TEST_SCHOOL_CLASS = SchoolClass(
    teacher=TEST_SCHOOL_STAFF_3, list_of_students={"Vova Ivanov", "Ivan Petrov"}
)
TEST_SCHOOL = SchoolCalculator()

class Staff:
    sum_of_salaries = 0

    def __init__(self, name, position, salary):
        self.name = name
        self.position = position
        self.salary = salary
        Staff.sum_of_salaries += self.salary

    def fire(self):
        if self in Staff:
            Staff.sum_of_salaries -= self.salary
            del self

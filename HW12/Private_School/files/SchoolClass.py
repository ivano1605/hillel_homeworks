class StudentsPresenceException(Exception):
    """This Student is in this class already"""


class SchoolClass:
    count_of_students_in_all_classes = 0

    def __init__(self, teacher, list_of_students):
        self.teacher = teacher
        self.list_of_students = list_of_students
        self.count_of_students = len(list_of_students)
        SchoolClass.count_of_students_in_all_classes += self.count_of_students

    def add_student(self, name):
        if name in self.list_of_students:
            raise StudentsPresenceException("This Student is in this class already")
        else:
            self.list_of_students.add(name)
            self.count_of_students += 1
            SchoolClass.count_of_students_in_all_classes += 1

    def exclude_student(self, name):
        if name not in self.list_of_students:
            raise ValueError("This Student is not in this class")
        else:
            self.list_of_students.remove(name)
            self.count_of_students -= 1
            SchoolClass.count_of_students_in_all_classes -= 1

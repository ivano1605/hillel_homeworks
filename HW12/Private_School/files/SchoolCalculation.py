from HW12.Private_School.files.SchoolClass import SchoolClass
from HW12.Private_School.files.Staff import Staff


class SchoolCalculator:
    def salary_budget(self):
        return Staff.sum_of_salaries

    def count_of_students(self):
        return SchoolClass.count_of_students_in_all_classes

    def monthly_payment_for_each_student(self):
        return self.salary_budget() / self.count_of_students()

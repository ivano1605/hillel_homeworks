from Staff import Staff


class School:
    def __init__(self, staff: [Staff] = None):
        self.salary_budget = 0
        self.staff = staff if staff else list()

    def __str__(self):
        return f"""
             Driver: {self.staff}
             
             """

    def add_staff(self, staff: [Staff]):
        self.salary_budget += staff.salary
        self.staff += staff


a = School()
a.add_staff(Staff("Director", "John", "Adams", 5000))

# print(a.staff)

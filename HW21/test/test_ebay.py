from HW21.fixtures.browser import browser_chrome

class Test_Ebay:
    def test_page_access(self, browser_chrome):
        ebay_link = "https://www.ebay.com/"
        browser_chrome.get(ebay_link)
        assert browser_chrome.current_url == ebay_link


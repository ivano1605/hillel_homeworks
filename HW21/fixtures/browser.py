import pytest
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture
def browser_chrome():
    chrome = Chrome(executable_path=ChromeDriverManager().install())

    chrome.maximize_window()
    yield chrome
    chrome.quit()

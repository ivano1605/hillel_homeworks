import pytest


@pytest.fixture
def petstore_headers():
    headers = {"accept": "application/json", "Content-Type": "application/json"}
    return headers


@pytest.fixture
def petstore_pet_info():
    pet_info = """
    {
      "id": 11,
      "name": "doggie",
      "category": {
        "id": 1,
        "name": "Dogs"
      },
      "photoUrls": [
        "string"
      ],
      "tags": [
        {
          "id": 0,
          "name": "string"
        }
      ],
      "status": "available"
    }
    """
    return pet_info


@pytest.fixture
def petstore_add_user():
    add_user = """
    {
  "id": 12,
  "username": "usernewunique12324",
  "firstName": "John",
  "lastName": "James",
  "email": "john@email.com",
  "password": "12345",
  "phone": "12345",
  "userStatus": 1
}
"""
    return add_user


@pytest.fixture
def petstore_update_user():
    update_user = """
{
  "id": 10,
  "username": "theUser",
  "firstName": "Igor",
  "lastName": "James",
  "email": "john@email.com",
  "password": "12345",
  "phone": "12345",
  "userStatus": 1
}
"""
    return update_user

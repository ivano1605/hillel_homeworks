import pytest
import requests
from HW13.pet_store.fixtures import *


def test_adding_new_pet(petstore_pet_info, petstore_headers):
    add_pet = requests.post(
        "https://petstore3.swagger.io/api/v3/pet",
        data=petstore_pet_info,
        headers=petstore_headers,
    )
    assert add_pet.status_code == 200


def test_find_pet_by_id(petstore_headers):
    find_pet = requests.get(
        "https://petstore3.swagger.io/api/v3/pet/11", headers=petstore_headers
    ).json()["id"]
    assert find_pet == 11

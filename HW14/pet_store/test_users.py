import pytest
import requests
from HW13.pet_store.fixtures import *


def test_adding_new_user(petstore_add_user, petstore_headers):
    add_user = requests.post(
        "https://petstore3.swagger.io/api/v3/user",
        data=petstore_add_user,
        headers=petstore_headers,
    )
    assert add_user.status_code == 200


def test_find_user_by_name(petstore_headers):
    find_user = requests.get(
        "https://petstore3.swagger.io/api/v3/user/user1", headers=petstore_headers
    ).json()["firstName"]
    assert find_user == "first name 1"
    print(find_user)


def test_update_user(petstore_update_user, petstore_headers):
    requests.put(
        "https://petstore3.swagger.io/api/v3/user/user1",
        data=petstore_update_user,
        headers=petstore_headers,
    )
    find_user = requests.get(
        "https://petstore3.swagger.io/api/v3/user/user1", headers=petstore_headers
    ).json()["firstName"]
    assert find_user == "first name 1"
    print(find_user)

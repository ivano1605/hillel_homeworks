import requests


def avg_temp(list_of_cities):
    list_of_temperatures = []
    for value in list_of_cities:
        list_of_temperatures.append(
            requests.get(
                f"https://api.openweathermap.org/data/2.5/weather?q={value}&units=metric&appid=15bca8c10e86424df00fa826657dca87"
            ).json()["main"]["temp"]
        )
    return sum(list_of_temperatures) / len(list_of_temperatures)


cities = [
    "kyiv",
    "minsk",
    "murmansk",
    "hamburg",
    "frankfurt",
    "munich",
    "rome",
    "liverpool",
    "manchester",
    "monaco",
]
print(f"Avg temperature in selected cities is {avg_temp(cities)} C")
